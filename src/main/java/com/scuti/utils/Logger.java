package com.scuti.utils;

public class Logger {
    public static void info(String text) {
        System.out.println("[INFO] ".concat(text));
    }

    public static void error(String text) {
        System.out.println("[ERROR] ".concat(text));
    }

    public static void line(String text) {
        System.out.println(text);
    }
}
