package com.scuti;

import com.scuti.database.Database;
import com.scuti.habbo.room.RoomManager;
import com.scuti.utils.Logger;

public class Emulator {
    private static Database db;
    private static RoomManager roomManager;
    public static void main(String[] args) {
        Logger.line("cc enft");
        Logger.line("");
        try {
            // Database
            db = new Database("localhost", "3306", "root", "", "scuti");
            Logger.info("Database -> OK");

            // Rooms
            roomManager = new RoomManager();

        } catch (Exception e) {
            Logger.error(e.getMessage());
        }
    }

    public static Database getDatabase() {
        return db;
    }
}
