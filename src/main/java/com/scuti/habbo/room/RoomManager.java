package com.scuti.habbo.room;

import com.scuti.Emulator;
import com.scuti.utils.Logger;

import java.util.HashMap;

public class RoomManager {
    private final HashMap<Integer, Room> rooms;
    public RoomManager() {
        this.rooms = new HashMap<>();
    }

    public void loadRooms() {
        try {

        } catch (Exception e) {
            Logger.error(e.getMessage());
        }
    }

    public HashMap<Integer, Room> getRooms() {
        return rooms;
    }
}
