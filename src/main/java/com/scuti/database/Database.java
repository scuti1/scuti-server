package com.scuti.database;

import com.scuti.utils.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    private Connection connection;
    public Database(String host, String port, String user, String password, String name) throws ClassNotFoundException, SQLException {
        String url = "jdbc:mysql://".concat(host).concat("/").concat(name);
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection(url, user, password);
    }

    public Connection getConnection() {
        return connection;
    }
}
